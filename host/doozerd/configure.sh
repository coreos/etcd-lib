#!/bin/sh

DIR=/var/run/etcd/doozerd
FILE="$DIR"/EnvironmentFile
USER_DATA=http://169.254.169.254/latest/user-data

mkdir -p $DIR

IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)

echo DOOZER_LISTEN=$IP > $FILE

CODE=$(curl -sL -w "%{http_code}" -o /dev/null $USER_DATA)

if [ $CODE != "404" ]; then
	curl -s $USER_DATA >> $FILE
	exit
fi

echo DOOZERD_MASTER=$IP >> $FILE

