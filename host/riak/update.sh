#!/bin/sh

IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)

while (( "$#" )); do
	if [ $1 == MASTER -a z"$2" != z"$IP" ]; then
		systemctl restart etcd@riak.service
	fi
	shift
done

