#!/bin/sh

DIR=/var/run/etcd/riak
FILE="$DIR"/EnvironmentFile
USER_DATA=http://169.254.169.254/latest/user-data

rm -Rf "$DIR"
mkdir "$DIR"

IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)
echo RIAK_IP=$IP > $FILE

master=$(etcd-client show --ns=riak MASTER | cut -f2 -d " ") || exit
if [ z"$master" != z -a z"$master" != z"$IP" ]; then
	echo RIAK_MASTER="$master" >> $FILE
fi
